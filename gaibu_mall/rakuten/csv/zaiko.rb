module GaibuMall::Rakuten::CSV
  class Zaiko < GaibuMall::Rakuten::ShouhinMaster::Renkei
    FILE_NAME = 'select.csv'

    def get_data
      [
        'n', #"項目選択肢用コントロールカラム", #0
        ec_sku_master.jisha_hinban, #    "商品管理番号（商品URL）", #1
        'i', # Not used by right on #    "選択肢タイプ", #2
        ec_sku_master.color_mei, #    "項目選択肢別在庫用横軸選択肢", #3
        ec_sku_master.color_cd, #    "項目選択肢別在庫用横軸選択肢子番号", #4
        ec_sku_master.size_mei, #    "項目選択肢別在庫用縦軸選択肢", #5
        ec_sku_master.size_cd, #    "項目選択肢別在庫用縦軸選択肢子番号", #6
        0, # Fixed #    "項目選択肢別在庫用取り寄せ可能表示", #7
        '0', # Fixed #    "項目選択肢別在庫用在庫数", #8
        0, # Fixed #    "在庫戻しフラグ", #9
        0, # Fixed #    "在庫切れ時の注文受付", #10
        '1000', # Fixed #    "在庫あり時納期管理番号", #11
        '', #    "在庫切れ時納期管理番号", #12
        '', # tag id ? and How to add multiple tag id ? #    "タグID", #13
        '' #    "画像URL" #14
     ]
    end

    def csv_file
      super { {path: csv_path, header: csv_header} }
    end

    private

    def csv_header
      super { FILE_NAME.split('.')[0] }
    end

    def csv_path
      super { FILE_NAME }
    end
  end
end