module GaibuMall::Rakuten::CSV
  class ShouhinCategory < GaibuMall::Rakuten::ShouhinMaster::Renkei
    FILE_NAME = 'item-cat.csv'

    def get_data
      [
        action, # コントロールカラム
        ec_shohin_master.jisha_hinban, # 商品管理番号（商品URL）
        "#{fetch_brand_mei} #{ec_shohin_master.ec_shohin_mei}<br><br>Right-on,ライトオン,#{ec_shohin_master.maker_hinban},#{fetch_brand_mei}", # 商品名
        '', # 表示先カテゴリ
        '1', # 優先度
        '' # 1ページ複数形式
      ]
    end

    def csv_file
      super { {path: csv_path, header: csv_header} }
    end

    private

    def csv_header
      super { FILE_NAME.split('.')[0] }
    end

    def csv_path
      super { FILE_NAME }
    end
  end
end