module GaibuMall::Rakuten::CSV
  class Shouhin < GaibuMall::Rakuten::ShouhinMaster::Renkei
    FILE_NAME = 'item.csv'

    def get_data
      [
        action, #"コントロールカラム", #0
        ec_shohin_master.jisha_hinban, #"商品管理番号（商品URL）", #1
        ec_shohin_master.jisha_hinban, #"商品番号", #2
        ec_shohin_master.rakuten_category_id, #"全商品ディレクトリID", #3
        '', # tag id ? and How to add multiple tag id ? #"タグID", #4
        "Right-on（ライトオン）  #{fetch_brand_mei}<br>", #"PC用キャッチコピー", #5
        "Right-on（ライトオン）", #"モバイル用キャッチコピー", #6
        "#{fetch_brand_mei} #{ec_shohin_master.ec_shohin_mei}<br><br>Right-on,ライトオン,#{ec_shohin_master.maker_hinban},#{fetch_brand_mei}", #"商品名", #7
        ec_shohin_master.genzai_baika_zeikomi || ec_shohin_master.baika_zeikomi_from_tree, #"販売価格", #8
        ec_shohin_master.genzai_baika_zeikomi || ec_shohin_master.baika_zeikomi_from_tree, #"表示価格", #9
        %Q(
          <div class='itempc1'>
          <ul>
          <li>#{ec_kihon_setsumei}</li>
          </ul>
          </div>
          <div class='itempc2'>
          <ul>
          <li>#{fetch_brand_mei}</lec_chui_jiko</li>
          </ul>
          </div>
        ), #"PC用商品説明文", #10
        %Q(
          <img src='#{ec_image_urls.dig(0,0)}' width='100%'><br>
          <table width='100%' border='0' cellspacing='0' cellpadding='15%' bgcolor='#F7F7F7'>
            #{
              img_html = []
              (0..Array(ec_image_urls[1..5]).count-1).collect do |i| # 2 to 6 images as per document provided by Umeda san
                img_html <<
                  <<-HTML
                    <tr>
                      <td align='center'>
                        #{ec_image_urls[i][1]}<br><br>
                        <img src='#{ec_image_urls[i][0]}' width='100%'>
                      </td>
                    </tr>
                  HTML
              end
              img_html.join('')
            }
          </table>
          <br><br>
          <center>Other image</center><br>
          <table width='100%' border='0' cellspacing='0' cellpadding='2' bgcolor='#F7F7F7'>
            #{
              img_html = []
              # unless @ec_image_urls[6..13].nil? # Enable in live
                # (6..@ec_image_urls[6..13].count-1).step(2) do |oi| # 7 to 14 images as per document provided by Umeda san # Enable in live
                (0..ec_image_urls.count-1).step(2) do |oi| # 7 to 14 images as per document provided by Umeda san
                  img_html << <<-HTML
                    <tr>
                      #{
                        in_img_html = []
                        2.times do |i|
                          in_img_html<<
                            if ec_image_urls[oi+i].present?
                              # if oi == 6 # Enable in live
                              if oi == 0
                                <<-HTML
                                  <td width='50%'><img src='#{ec_image_urls[oi+i][0]}' width='100%'></td>
                                HTML
                              else
                                <<-HTML
                                  <td><img src='#{ec_image_urls[oi+i][0]}' width='100%'></td>
                                HTML
                              end
                            end
                        end
                        in_img_html.join('')
                      }
                    </tr>
                  HTML
                end
              # end
              img_html.join('')
            }
          </table>
          <br><hr><br><br>
          #{ec_kihon_setsumei}
          <br>
          #{ec_chui_jiko}
        ), #"スマートフォン用商品説明文", #11
        %Q(
          <div id='item_wrap'>
          <h2>#{fetch_brand_mei} #{ec_shohin_master.ec_shohin_mei}</h2>
          <div class='ef_main'><img src='#{ec_image_urls.dig(0,0)}'></div>
          <div id='zigzag'>
          <!--zigzag-->
          #{
            img_html = []
            (0..Array(ec_image_urls[0..4]).count-1).collect do |i| # 1 to 5 images as per document provided by Umeda san
              img_html << if (i+1)%2 == 1
                <<-HTML
                  <div class='block'>
                  <div class='ef_l'><img src='#{ec_image_urls[i][0]}'></div>
                  <div class='ef_lt'>
                  <p>Point</p>#{ec_image_urls[i][1]}</div>
                  </div><div style='clear:both'></div>
                HTML
              else
                <<-HTML
                  <div class='block'>
                  <div class='ef_r'><img src='#{ec_image_urls[i][0]}'></div>
                  <div class='ef_rt'>
                  <p>Point</p>#{ec_image_urls[i][1]}</div>
                  </div><div style='clear:both'></div>
                HTML
              end
            end
            img_html.join('<br>')
          }
          <!--other-->
          <div class='other_img clearfix'>
          <p>Other image</p>
          <ul>
            #{
              img_html = []
              (5..Array(ec_image_urls[5..ec_image_urls.count-1]).count).collect do |i| # 5 to 14 images as per document provided by Umeda san
              # (0..@ec_image_urls.count-1).collect do |i|
                img_html << <<-HTML
                  <li><figure><a href='#{ec_image_urls[i][0]}' target='_blank'><img src='#{ec_image_urls[i][0]}'></a></figure></li>
                HTML
              end
              img_html.join('')
            }
          </ul>
          <div style='clear:both'></div>
          </div>
          <!--comment-->
          <div class='i_comment clearfix'>
          <p>#{ec_kihon_setsumei}</p>
          <p>#{ec_chui_jiko}</p>
          </div>
          </div>
          </div>
        ), #"PC用販売説明文", #12
        "#{ec_image_urls.map(&:first).join(' ')}", #"商品画像URL", #13
        '', # No need to register. Not in live #"カタログID", #14
        '4', # Fixed #"カタログIDなしの理由", #15
        '2', # Fixed #"在庫タイプ", #16
        'カラー', # Fixed #"項目選択肢別在庫用横軸項目名", #17
        'サイズ', # Fixed #"項目選択肢別在庫用縦軸項目名", #18
        '0', # Fixed #"項目選択肢別在庫用残り表示閾値", #19
        '' # Auto set #"白背景画像URL" #20
      ]
    end

    def csv_file
      super { {path: csv_path, header: csv_header} }
    end

    private

    def csv_header
      super { FILE_NAME.split('.')[0] }
    end

    def csv_path
      super { FILE_NAME }
    end

    def ec_image_urls
      ec_image_urls ||= []
    end

    def ec_kihon_setsumei
      ActionView::Base.full_sanitizer.sanitize(ec_shohin_master&.ec_kihon_setsumei || ec_shohin_master&.description || ec_shohin_master.catch_copy.to_s + ec_shohin_master.kihon_setsumei.to_s || '')
    end

    def ec_chui_jiko
      ActionView::Base.full_sanitizer.sanitize(ec_shohin_master&.new_ec_chui_jiko.presence|| ec_shohin_master&.handling_precautions.presence || ec_shohin_master.old_ec_chui_jiko || '')
    end
  end
end