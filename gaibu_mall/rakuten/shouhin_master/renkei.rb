require 'net/ftp'

module GaibuMall::Rakuten::ShouhinMaster
  class Renkei
    cattr_accessor :ec_shohin_master, :ec_sku_master
    attr_accessor :shouhin, :shouhin_cat, :zaiko

    def initialize(jisha_hinbans, **args)
      @jisha_hinbans = jisha_hinbans
      @args = args
    end

    def bulk_write
      shouhin = GaibuMall::Rakuten::CSV::Shouhin.new(@jisha_hinbans, @args)
      shouhin_cat = GaibuMall::Rakuten::CSV::ShouhinCategory.new(@jisha_hinbans, @args)
      zaiko = GaibuMall::Rakuten::CSV::Zaiko.new(@jisha_hinbans, @args)

      # Open file to write
      shouhin_file = shouhin.csv_file
      shouhin_cat_file = shouhin_cat.csv_file
      ec_shohin_masters do |shohin_master|
        self.ec_shohin_master = shohin_master
        shouhin_file << shouhin.get_data
        shouhin_cat_file << shouhin_cat.get_data
      end
      # Close file after write all data
      shouhin_file.close()
      shouhin_cat_file.close()

      zaiko_file = zaiko.csv_file
      # WORKING.....
      ec_sku_masters do |sku|
        self.ec_sku_master = sku
        zaiko_file << zaiko.get_data
      end
      # Close file after write all data
      zaiko_file.close()
    end

    def send_via_ftp
      ftp = Net::FTP.open(AppConfig.find_by(key: 'RK_FTP_HOST').value, Rails.application.secrets.rk_ftp_username, Rails.application.secrets.rk_ftp_password) do |fp|
        fp.chdir('/ritem/batch/')
        fp.putbinaryfile(shouhin.send(:csv_path))
        fp.putbinaryfile(shouhin_cat.send(:csv_path))
        fp.putbinaryfile(zaiko.send(:csv_path))
      end
    end

    module API
    end

    module CSV
      module_function
      def new(jisha_hinbans, **args)
        jisha_hinbans = Array(jisha_hinbans)
        Renkei.new(jisha_hinbans, **args)
      end
    end

    private

    def shouhin
      @shouhin || GaibuMall::Rakuten::CSV::Shouhin.new(@jisha_hinbans)
    end

    def shouhin_cat
      @shouhin_cat || GaibuMall::Rakuten::CSV::ShouhinCategory.new(@jisha_hinbans)
    end

    def zaiko
      @zaiko || GaibuMall::Rakuten::CSV::Zaiko.new(@jisha_hinbans)
    end

    def rakuten_csv_fields
      @rakuten_csv_fields ||= RakutenCSVField.select(:file_type, :field_name).all.as_json
    end

    def action
      case ec_shohin_master.rakuten_renkei_flag.to_sym
      when :rk_renkei_create then 'n'
      when :rk_renkei_update then 'u'
      when :rk_renkei_delete then 'd'
      when :rk_renkei_not_yet then 'n'
      end
    end

    def fetch_brand_mei
      ec_shohin_master.ec_brand_mei.presence || ec_shohin_master.roring_brand_mei.presence || ec_shohin_master.tree_ec_brand_mei
    end

    def ec_shohin_masters
      MEcShohinMaster.active.select("distinct
          m_ec_shohin_masters.id,
          m_ec_shohin_masters.jisha_hinban,
          m_ec_shohin_masters.maker_hinban,
          m_ec_shohin_masters.item_cd,
          m_ec_shohin_masters.rakuten_renkei_flag,
          m_ec_shohin_masters.catch_copy,
          m_ec_shohin_masters.kihon_setsumei,
          m_ec_shohin_masters.ec_chui_jiko old_ec_chui_jiko,
          m_ec_shohin_masters.ec_shohin_mei,
          m_ec_shohin_site_detail_masters.rakuten_category_id,
          m_ec_brand_masters.ec_brand_mei ec_brand_mei,
          roring_brands_m_ec_shohin_masters.ec_brand_mei roring_brand_mei,
          shohin_descriptions.ec_chui_jiko new_ec_chui_jiko,
          shohin_descriptions.ec_kihon_setsumei,
          shohin_descriptions.description,
          shohin_descriptions.handling_precautions,
          m_hinban_genzai_baika_zeikomis.genzai_baika_zeikomi,
          m_shohin_tree_masters.baika_zeikomi baika_zeikomi_from_tree,
          brands_m_shohin_tree_masters.ec_brand_mei tree_ec_brand_mei"
        ).joins(
          :m_ec_shohin_site_detail_master,
          :brand,
          :roring_brand,
          :m_hinban_genzai_baika_zeikomi,
          m_shohin_tree_master: [:shohin_description, :brand]
        ).where(jisha_hinban: @jisha_hinbans).find_in_batches(batch_size: 200) do |shouhins|
        shouhins.each do |shouhin|
          yield shouhin if block_given?
        end
      end
    end

    def ec_sku_masters
      MEcShohinSkuSiteDetailMaster.active.select(
        :id,
        :jisha_hinban,
        :color_mei,
        :color_cd,
        :size_mei,
        :size_cd
      ).where(jisha_hinban: @jisha_hinbans).find_in_batches(batch_size: 200) do |skus|
        skus.each do |sku|
          yield sku if block_given?
        end
      end
    end

    def csv_file
      ::CSV.open(yield[:path], 'w', write_headers: true, headers: yield[:header], encoding: 'Shift_JIS')
    end

    def csv_header
      rakuten_csv_fields.select { |f| f['file_type'] == yield }.pluck('field_name')
    end

    def csv_path
      dir = "#{Rails.root}/tmp/rakuten_csv"
      Dir.mkdir(dir) unless File.exists?(dir)
      File.new("#{dir}/#{yield}")
    end
  end
end