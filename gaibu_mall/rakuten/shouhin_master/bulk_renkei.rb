module GaibuMall::Rakuten::ShouhinMaster
  class BulkRenkei
    def self.call(**args)
      self.new(args).call
    end

    def initialize(**args)
      @jisha_hinbans = args[:jisha_hinbans] || MEcShohinMaster.where(updated_at: Time.zone.now.at_beginning_of_day..Time.zone.now.at_end_of_day, rakuten_site_tenkai_flag: 1).pluck(:jisha_hinban)
    end

    def call
      jisha_hinbans = Array(@jisha_hinbans)
      csv_renkei = GaibuMall::Rakuten::ShouhinMaster::Renkei::CSV.new(jisha_hinbans)
      csv_renkei.bulk_write
      csv_renkei.send_via_ftp
    end
  end
end