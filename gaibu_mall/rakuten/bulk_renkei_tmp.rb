module GaibuMall::Rakuten
  module ShouhinMaster
    class BulkRenkei
      def self.call(**args)
        self.new(args).call
      end

      def initialize(**args)
        @jisha_hinbans = args[:jisha_hinbans]
      end

      def call
        jisha_hinbans = Array(@jisha_hinbans) || MEcShohinMaster.select(:jisha_hinban).where(updated_at: Time.zone.now.at_beginning_of_day..Time.zone.now.at_end_of_day, rakuten_site_tenkai_flag: 1)
        csv_remkei = GaibuMall::Rakuten::ShouhinMaster::Renkei::CSV.new(jisha_hinbans)
        csv_remkei.bulk_write
        csv_remkei.send_via_ftp
      end
    end
  end
end